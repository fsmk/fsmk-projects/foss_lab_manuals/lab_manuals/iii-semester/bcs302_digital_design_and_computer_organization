`timescale 1ns/1ps

module tb_subtractors;

  // Parameters
  parameter SIM_TIME = 50; // Simulation time

  // Signals
  reg a, b, bin;
  wire diff_hs, bout_hs, diff_fs, bout_fs;

  // Instantiate half subtractor and full subtractor
  half_subtractor hs_inst (.a(a), .b(b), .diff(diff_hs), .bout(bout_hs));
  full_subtractor fs_inst (.a(a), .b(b), .bin(bin), .diff(diff_fs), .bout(bout_fs));

  // Initial block for stimulus
  initial begin
    // Test case 1: a=1, b=0, bin=0
    a = 1; b = 0; bin = 0;
    #10;

    // Test case 2: a=0, b=1, bin=0
    a = 0; b = 1; bin = 0;
    #10;

    // Test case 3: a=1, b=1, bin=0
    a = 1; b = 1; bin = 0;
    #10;

    // Test case 4: a=1, b=1, bin=1
    a = 1; b = 1; bin = 1;
    #10;

    // End simulation
    $finish;
  end

  // Display results
  always @(posedge bout_hs or posedge bout_fs) begin
    $display("Time=%0t: a=%b, b=%b, bin=%b, Diff_HS=%b, Bout_HS=%b, Diff_FS=%b, Bout_FS=%b",
             $time, a, b, bin, diff_hs, bout_hs, diff_fs, bout_fs);
  end

  // Dump variables to VCD file
  initial begin
    $dumpfile("tb_subtractors.vcd");
    $dumpvars(0, tb_subtractors);
  end

endmodule
