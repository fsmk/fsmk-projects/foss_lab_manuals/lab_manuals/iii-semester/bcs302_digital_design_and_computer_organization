`timescale 1ns/1ps

module tb_sr_flipflop;

  // Parameters
  parameter SIM_TIME = 50; // Simulation time

  // Signals
  reg s, r, clk;
  wire q, q_bar;

  // Instantiate the SR Flip-Flop
  sr_flipflop flipflop_inst (.s(s), .r(r), .clk(clk), .q(q), .q_bar(q_bar));

  // Initial block for stimulus
  initial begin
    // Test case 1: Set the flip-flop (s=1, r=0)
    s = 1; r = 0; clk = 0;
    #10; // Wait for a few clock cycles

    // Test case 2: Clock transitions (clk=1)
    s = 0; r = 0; clk = 1;
    #10;

    // Test case 3: Reset the flip-flop (s=0, r=1)
    s = 0; r = 1; clk = 0;
    #10;

    // Test case 4: Clock transitions (clk=1)
    s = 0; r = 0; clk = 1;
    #10;

    // Test case 5: No Set or Reset (s=0, r=0)
    s = 0; r = 0; clk = 0;
    #10;

    // End simulation
    $finish;
  end

  // Display results
  always @(posedge clk) begin
    $display("Time=%0t: s=%b, r=%b, clk=%b, q=%b, q_bar=%b", $time, s, r, clk, q, q_bar);
  end

  // Dump variables to VCD file
  initial begin
    $dumpfile("tb_sr_flipflop.vcd");
    $dumpvars(0, tb_sr_flipflop);
  end


endmodule
