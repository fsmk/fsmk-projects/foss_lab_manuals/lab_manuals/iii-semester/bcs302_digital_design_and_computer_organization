`timescale 1ns/1ps

module tb_decimal_adder;

  // Parameters
  parameter SIM_TIME = 50; // Simulation time

  // Signals
  reg [3:0] a, b;
  wire [4:0] sum;

  // Instantiate the decimal adder
  decimal_adder da_inst (.a(a), .b(b), .sum(sum));

  // Initial block for stimulus
  initial begin
    // Test case 1: a=5, b=3
    a = 5; b = 3;
    #10;

    // Test case 2: a=9, b=2
    a = 9; b = 2;
    #10;

    // Test case 3: a=8, b=7
    a = 8; b = 7;
    #10;

    // Test case 4: a=1, b=1
    a = 1; b = 1;
    #10;

    // End simulation
    $finish;
  end

  // Display results
  always @(posedge sum) begin
    $display("Time=%0t: a=%d, b=%d, Sum=%d", $time, a, b, sum);
  end

  // Dump variables to VCD file
  initial begin
    $dumpfile("tb_decimal_adder.vcd");
    $dumpvars(0, tb_decimal_adder);
  end


endmodule
