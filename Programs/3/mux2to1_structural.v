// Structural Model - 2-to-1 Multiplexer

module mux2to1_structural(input a, b, sel, output y);
  wire w1, w2;

  // Instantiate AND gates and OR gate
  andgate and1(.a(a), .b(~sel), .y(w1));
  andgate and2(.a(b), .b(sel), .y(w2));
  orgate or1(.a(w1), .b(w2), .y(y));

endmodule

module andgate(input a, b, output y);
  assign y = a & b;
endmodule

module orgate(input a, b, output y);
  assign y = a | b;
endmodule
