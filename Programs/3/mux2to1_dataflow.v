// Data Flow Model - 2-to-1 Multiplexer

module mux2to1_dataflow(input a, b, sel, output y);
  assign y = (sel) ? b : a;
endmodule
