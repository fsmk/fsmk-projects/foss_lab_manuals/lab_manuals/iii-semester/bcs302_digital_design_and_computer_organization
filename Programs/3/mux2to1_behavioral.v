// Behavioral Model - 2-to-1 Multiplexer

module mux2to1_behavioral(input a, b, sel, output reg y);
  always @(a, b, sel) begin
    if (sel)
      y = b;
    else
      y = a;
  end
endmodule
