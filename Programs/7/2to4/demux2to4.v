module demux2to4 (
  input wire [1:0] in,
  input wire [1:0] sel,
  output wire out0,
  output wire out1,
  output wire out2,
  output wire out3
);

  assign out0 = (sel == 2'b00) ? (in == 2'b00) : 1'b0;
  assign out1 = (sel == 2'b01) ? (in == 2'b01) : 1'b0;
  assign out2 = (sel == 2'b10) ? (in == 2'b10) : 1'b0;
  assign out3 = (sel == 2'b11) ? (in == 2'b11) : 1'b0;

endmodule
