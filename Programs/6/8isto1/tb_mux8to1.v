`timescale 1ns/1ps

module tb_mux8to1;

  // Parameters
  parameter SIM_TIME = 50; // Simulation time

  // Signals
  reg [7:0] a, b, c, d, e, f, g, h;
  reg [2:0] sel;
  wire [7:0] y;

  // Instantiate the 8:1 Multiplexer
  mux8to1 mux_inst (.a(a), .b(b), .c(c), .d(d), .e(e), .f(f), .g(g), .h(h), .sel(sel), .y(y));

  // Initial block for stimulus
  initial begin
    // Test case 1: sel=3'b000, a=8'b10101010, b=8'b11001100, c=8'b00110011, d=8'b01100110, e=8'b11110000, f=8'b00001111, g=8'b11111111, h=8'b00000000
    sel = 3'b000; a = 8'b10101010; b = 8'b11001100; c = 8'b00110011; d = 8'b01100110; e = 8'b11110000; f = 8'b00001111; g = 8'b11111111; h = 8'b00000000;
    #10;

    // Test case 2: sel=3'b001, a=8'b01010101, b=8'b11110000, c=8'b00001111, d=8'b10101010, e=8'b11001100, f=8'b00110011, g=8'b01100110, h=8'b11111111
    sel = 3'b001; a = 8'b01010101; b = 8'b11110000; c = 8'b00001111; d = 8'b10101010; e = 8'b11001100; f = 8'b00110011; g = 8'b01100110; h = 8'b11111111;
    #10;

    // Test case 3: sel=3'b010, a=8'b10011001, b=8'b01010101, c=8'b11001100, d=8'b00110011, e=8'b11111111, f=8'b00000000, g=8'b11110000, h=8'b01100110
    sel = 3'b010; a = 8'b10011001; b = 8'b01010101; c = 8'b11001100; d = 8'b00110011; e = 8'b11111111; f = 8'b00000000; g = 8'b11110000; h = 8'b01100110;
    #10;

    // Test case 4: sel=3'b111, a=8'b01100110, b=8'b11111111, c=8'b00000000, d=8'b10101010, e=8'b11001100, f=8'b00110011, g=8'b11110000, h=8'b01010101
    sel = 3'b111; a = 8'b01100110; b = 8'b11111111; c = 8'b00000000; d = 8'b10101010; e = 8'b11001100; f = 8'b00110011; g = 8'b11110000; h = 8'b01010101;
    #10;

    // End simulation
    $finish;
  end

  // Display results
  always @(posedge sel or posedge a or posedge b or posedge c or posedge d or posedge e or posedge f or posedge g or posedge h) begin
    $display("Time=%0t: sel=%b, a=%h, b=%h, c=%h, d=%h, e=%h, f=%h, g=%h, h=%h, y=%h", $time, sel, a, b, c, d, e, f, g, h, y);
  end

  // Dump variables to VCD file
  initial begin
    $dumpfile("tb_mux8to1.vcd");
    $dumpvars(0, tb_mux8to1);
  end

endmodule
