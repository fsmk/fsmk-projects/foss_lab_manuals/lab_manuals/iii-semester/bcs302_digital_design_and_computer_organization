`timescale 1ns/1ps

module tb_mux4to1;

  // Parameters
  parameter SIM_TIME = 50; // Simulation time

  // Signals
  reg [3:0] a, b, c, d;
  reg [1:0] sel;
  wire [3:0] y;

  // Instantiate the 4:1 Multiplexer
  mux4to1 mux_inst (.a(a), .b(b), .c(c), .d(d), .sel(sel), .y(y));

  // Initial block for stimulus
  initial begin
    // Test case 1: sel=2'b00, a=4'b1010, b=4'b1100, c=4'b0011, d=4'b0110
    sel = 2'b00; a = 4'b1010; b = 4'b1100; c = 4'b0011; d = 4'b0110;
    #10;

    // Test case 2: sel=2'b01, a=4'b0101, b=4'b1111, c=4'b0000, d=4'b1010
    sel = 2'b01; a = 4'b0101; b = 4'b1111; c = 4'b0000; d = 4'b1010;
    #10;

    // Test case 3: sel=2'b10, a=4'b1001, b=4'b0101, c=4'b1100, d=4'b0011
    sel = 2'b10; a = 4'b1001; b = 4'b0101; c = 4'b1100; d = 4'b0011;
    #10;

    // Test case 4: sel=2'b11, a=4'b0011, b=4'b1110, c=4'b1101, d=4'b0000
    sel = 2'b11; a = 4'b0011; b = 4'b1110; c = 4'b1101; d = 4'b0000;
    #10;

    // End simulation
    $finish;
  end

  // Display results
  always @(posedge sel or posedge a or posedge b or posedge c or posedge d) begin
    $display("Time=%0t: sel=%b, a=%b, b=%b, c=%b, d=%b, y=%b", $time, sel, a, b, c, d, y);
  end

  // Dump variables to VCD file
  initial begin
    $dumpfile("tb_mux4to1.vcd");
    $dumpvars(0, tb_mux4to1);
  end


endmodule
